# Permissions and privacy

Updated April 12, 2022

## Disclosure

Use of information received from Atlassian Jira APIs will adhere to [Atlassian Developer Terms](https://developer.atlassian.com/platform/marketplace/atlassian-developer-terms/) and [Google Chrome Web Store Developer Program Policies](https://developer.chrome.com/docs/webstore/program_policies/), including the Limited Use requirements.

## Limited Use

**Authentication information:** This extension use browser cookies (issued by Jira login) authenticate users, but none of this information is collected or stored by developer.

**User Activity:** Only anonymous usage information is analyzed including but not limited to how prevalent the use of certain Product features is so that we can optimize the Products. An example is which options and buttons users click most often.

**Atlassian user data:** Atlassian user data is accessed by this extension solely in order to display it to the user via the popup window, which is the purpose of this extension. For performance reasons, some information is stored locally in the extensions and can be removed by uninstalling the extension. For example: When you load extension for first time, you selected Jira filter is saved in your browser safely, so it can used to re-open last active filter selection quickly. Your Atlassian user data is not shared with anyone else other than You (the extension user). The extensions act as a conduit to the user's Atlassian data.

## Permissions

The permissions You agree to when installing the extensions are the minimum requirements for the extensions to perform their basic purpose.

### Got Questions or feedback?

Please [email me](mailto:jawandarajbir@gmail.com) your questions or feedback.
