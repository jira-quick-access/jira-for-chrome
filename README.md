# Jira for Chrome

How long does it take currently for you to access your work in Jira?
10 seconds? 15 seconds? longer 🤯 ?

Now you can access your work in just 1 second ⚡️
Don't believe it!! Try Jira for Chrome to find out yourself 🥳

---

Presenting Quick and Easy access to your work in Jira Cloud

[hyperlink0]: https://chrome.google.com/webstore/detail/jira-quick-access/emekjbfbbnnijlenmnbngkikcmdaelno
[image0]: https://bitbucket.org/jira-quick-access/jira-quick-access-src/raw/de96c7be7231c62d3c0f5183cfe53681514a0786/builds%20published/chrome-store-badge.png "Install from Chrome Web Store"

[![alt text][image2]][hyperlink2]

[hyperlink2]: https://chrome.google.com/webstore/detail/jira-quick-access/emekjbfbbnnijlenmnbngkikcmdaelno/reviews
[image2]: https://img.shields.io/chrome-web-store/rating/emekjbfbbnnijlenmnbngkikcmdaelno?label=chrome%20web%20store%20rating "Chrome Web Store ratings"

[![alt text][image3]][hyperlink3]

[hyperlink3]: https://chrome.google.com/webstore/detail/jira-quick-access/emekjbfbbnnijlenmnbngkikcmdaelno
[image3]: https://img.shields.io/chrome-web-store/v/emekjbfbbnnijlenmnbngkikcmdaelno?label=published%20version "Chrome Web Store published version"

## Features 🎉

Boost your Productivity working with Jira Cloud by enabling quick access to:

- Open Issues assigned to your in Jira
- Recently viewed issues in Jira
- Jira filters you have created in Jira
- Shared filters that you have marked as favourite in Jira
- Switch between multiple Jira Products (linked to your account)
- Your Atlassian profile and Account settings
- Create a new Jira issue

## Supports 🤝

The following products in Jira Cloud are supported:

- Jira Software
- Jira Work Management
- Jira Service Management

Jira Server on custom domains is not currently supported.

## Coming up next 🚧

- Add Search feature to lookup Jira ticket across all your Jira instances
- Support for more browsers
- Performance improvements

### Got Questions or Feedback? 🙋

Please [email me](mailto:jawandarajbir@gmail.com) or [post here](https://chrome.google.com/webstore/detail/jira-quick-access/emekjbfbbnnijlenmnbngkikcmdaelno/support) your questions or feedback.
